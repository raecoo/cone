# Easiest way to run Cone::Web.
# Run with "bundle exec rackup simple.ru"

require 'cone'

# A Web process always runs as client, no need to configure server
Cone.configure_client do |config|
  config.redis = { url: 'redis://localhost:6379/0', size: 1, namespace: 'foo' }
end

Cone::Client.push('class' => "HardWorker", 'args' => [])

require 'cone/web'
run Cone::Web
