Cone.configure_client do |config|
  config.redis = { :size => 2, :namespace => 'foo' }
end
Cone.configure_server do |config|
  config.redis = { :namespace => 'foo' }
  config.on(:startup) { }
  config.on(:quiet) { }
  config.on(:shutdown) do
    #result = RubyProf.stop

    ## Write the results to a file
    ## Requires railsexpress patched MRI build
    # brew install qcachegrind
    #File.open("callgrind.profile", "w") do |f|
      #RubyProf::CallTreePrinter.new(result).print(f, :min_percent => 1)
    #end
  end
end

require 'cone/web'
Cone::Web.app_url = '/'

class EmptyWorker
  include Cone::Worker

  def perform
  end
end

class TimedWorker
  include Cone::Worker

  def perform(start)
    now = Time.now.to_f
    puts "Latency: #{now - start} sec"
  end
end
