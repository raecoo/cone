require_relative 'helper'

$HAS_AJ = true
begin
  require 'active_job'
rescue LoadError
  $HAS_AJ = false
end

class TestRails < Cone::Test

  describe 'ActiveJob' do
    it 'does not allow Cone::Worker in AJ::Base classes' do
      ex = assert_raises ArgumentError do
        c = Class.new(ActiveJob::Base)
        c.send(:include, Cone::Worker)
      end
      assert_includes ex.message, "cannot include"
    end if $HAS_AJ
  end
end
