require_relative 'helper'

require 'active_record'
require 'cone/rails'

Cone.hook_rails!

class TestTesting < Cone::Test
  describe 'cone testing' do
    describe 'require/load cone/testing.rb' do
      before do
        require 'cone/testing'
      end

      after do
        Cone::Testing.disable!
      end

      it 'enables fake testing' do
        Cone::Testing.fake!
        assert Cone::Testing.enabled?
        assert Cone::Testing.fake?
        refute Cone::Testing.inline?
      end

      it 'enables fake testing in a block' do
        Cone::Testing.disable!
        assert Cone::Testing.disabled?
        refute Cone::Testing.fake?

        Cone::Testing.fake! do
          assert Cone::Testing.enabled?
          assert Cone::Testing.fake?
          refute Cone::Testing.inline?
        end

        refute Cone::Testing.enabled?
        refute Cone::Testing.fake?
      end

      it 'disables testing in a block' do
        Cone::Testing.fake!
        assert Cone::Testing.fake?

        Cone::Testing.disable! do
          refute Cone::Testing.fake?
          assert Cone::Testing.disabled?
        end

        assert Cone::Testing.fake?
        assert Cone::Testing.enabled?
      end
    end

    describe 'require/load cone/testing/inline.rb' do
      before do
        require 'cone/testing/inline'
      end

      after do
        Cone::Testing.disable!
      end

      it 'enables inline testing' do
        Cone::Testing.inline!
        assert Cone::Testing.enabled?
        assert Cone::Testing.inline?
        refute Cone::Testing.fake?
      end

      it 'enables inline testing in a block' do
        Cone::Testing.disable!
        assert Cone::Testing.disabled?
        refute Cone::Testing.fake?

        Cone::Testing.inline! do
          assert Cone::Testing.enabled?
          assert Cone::Testing.inline?
        end

        refute Cone::Testing.enabled?
        refute Cone::Testing.inline?
        refute Cone::Testing.fake?
      end
    end
  end

  describe 'with middleware' do
    before do
      require 'cone/testing'
    end

    after do
      Cone::Testing.disable!
    end

    class AttributeWorker
      include Cone::Worker
      class_attribute :count
      self.count = 0
      attr_accessor :foo

      def perform
        self.class.count += 1 if foo == :bar
      end
    end

    class AttributeMiddleware
      def call(worker, msg, queue)
        worker.foo = :bar if worker.respond_to?(:foo=)
        yield
      end
    end

    it 'wraps the inlined worker with middleware' do
      Cone::Testing.server_middleware do |chain|
        chain.add AttributeMiddleware
      end

      begin
        Cone::Testing.fake! do
          AttributeWorker.perform_async
          assert_equal 0, AttributeWorker.count
        end

        AttributeWorker.perform_one
        assert_equal 1, AttributeWorker.count

        Cone::Testing.inline! do
          AttributeWorker.perform_async
          assert_equal 2, AttributeWorker.count
        end
      ensure
        Cone::Testing.server_middleware.clear
      end
    end
  end

end
