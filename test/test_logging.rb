require_relative 'helper'
require 'cone/logging'

class TestLogging < Cone::Test
  describe Cone::Logging do
    describe "#with_context" do
      def context
        Cone::Logging.logger.formatter.context
      end

      it "has no context by default" do
        context.must_equal nil
      end

      it "can add a context" do
        Cone::Logging.with_context "xx" do
          context.must_equal " xx"
        end
        context.must_equal nil
      end

      it "can use multiple contexts" do
        Cone::Logging.with_context "xx" do
          context.must_equal " xx"
          Cone::Logging.with_context "yy" do
            context.must_equal " xx yy"
          end
          context.must_equal " xx"
        end
        context.must_equal nil
      end
    end
  end
end
