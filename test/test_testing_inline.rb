require_relative 'helper'

require 'active_record'
require 'cone/rails'

Cone.hook_rails!

class TestInline < Cone::Test
  describe 'cone inline testing' do
    class InlineError < RuntimeError; end
    class ParameterIsNotString < RuntimeError; end

    class InlineWorker
      include Cone::Worker
      def perform(pass)
        raise ArgumentError, "no jid" unless jid
        raise InlineError unless pass
      end
    end

    class InlineWorkerWithTimeParam
      include Cone::Worker
      def perform(time)
        raise ParameterIsNotString unless time.is_a?(String) || time.is_a?(Numeric)
      end
    end

    # class InlineFooMailer < ActionMailer::Base
    #   def bar(str)
    #     raise InlineError
    #   end
    # end

    class InlineFooModel < ActiveRecord::Base
      def self.bar(str)
        raise InlineError
      end
    end

    before do
      require 'cone/testing/inline'
      Cone::Testing.inline!
    end

    after do
      Cone::Testing.disable!
    end

    it 'stubs the async call when in testing mode' do
      assert InlineWorker.perform_async(true)

      assert_raises InlineError do
        InlineWorker.perform_async(false)
      end
    end

    it 'stubs the enqueue call when in testing mode' do
      assert Cone::Client.enqueue(InlineWorker, true)

      assert_raises InlineError do
        Cone::Client.enqueue(InlineWorker, false)
      end
    end

    it 'stubs the push_bulk call when in testing mode' do
      assert Cone::Client.push_bulk({'class' => InlineWorker, 'args' => [[true], [true]]})

      assert_raises InlineError do
        Cone::Client.push_bulk({'class' => InlineWorker, 'args' => [[true], [false]]})
      end
    end

    it 'should relay parameters through json' do
      assert Cone::Client.enqueue(InlineWorkerWithTimeParam, Time.now)
    end

  end
end
