require_relative 'helper'
require 'cone/launcher'

class TestLauncher < Cone::Test

  describe 'launcher' do
    before do
      Cone.redis {|c| c.flushdb }
    end

    def new_manager(opts)
      Cone::Manager.new(opts)
    end

    describe 'heartbeat' do
      before do
        uow = Object.new

        @mgr = new_manager(options)
        @launcher = Cone::Launcher.new(options)
        @launcher.manager = @mgr

        Cone::Processor::WORKER_STATE['a'] = {'b' => 1}

        @proctitle = $0
      end

      after do
        Cone::Processor::WORKER_STATE.clear
        $0 = @proctitle
      end

      describe 'when manager is active' do
        before do
          Cone::CLI::PROCTITLES << proc { "xyz" }
          @launcher.heartbeat('identity', heartbeat_data, Cone.dump_json(heartbeat_data))
          Cone::CLI::PROCTITLES.pop
        end

        it 'sets useful info to proctitle' do
          assert_equal "cone #{Cone::VERSION} myapp [1 of 3 busy] xyz", $0
        end

        it 'stores process info in redis' do
          info = Cone.redis { |c| c.hmget('identity', 'busy') }
          assert_equal ["1"], info
          expires = Cone.redis { |c| c.pttl('identity') }
          assert_in_delta 60000, expires, 500
        end
      end

      describe 'when manager is stopped' do
        before do
          @launcher.quiet
          @launcher.heartbeat('identity', heartbeat_data, Cone.dump_json(heartbeat_data))
        end

        it 'indicates stopping status in proctitle' do
          assert_equal "cone #{Cone::VERSION} myapp [1 of 3 busy] stopping", $0
        end

        it 'stores process info in redis' do
          info = Cone.redis { |c| c.hmget('identity', 'busy') }
          assert_equal ["1"], info
          expires = Cone.redis { |c| c.pttl('identity') }
          assert_in_delta 60000, expires, 50
        end
      end
    end

    def options
      { :concurrency => 3, :queues => ['default'] }
    end

    def heartbeat_data
      { 'concurrency' => 3, 'tag' => 'myapp' }
    end
  end

end
