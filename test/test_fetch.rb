require_relative 'helper'
require 'cone/fetch'

class TestFetcher < Cone::Test
  describe 'fetcher' do
    before do
      Cone.redis = { :url => REDIS_URL, :namespace => 'fuzzy' }
      Cone.redis do |conn|
        conn.flushdb
        conn.rpush('queue:basic', 'msg')
      end
    end

    after do
      Cone.redis = REDIS
    end

    it 'retrieves' do
      fetch = Cone::BasicFetch.new(:queues => ['basic', 'bar'])
      uow = fetch.retrieve_work
      refute_nil uow
      assert_equal 'basic', uow.queue_name
      assert_equal 'msg', uow.job
      q = Cone::Queue.new('basic')
      assert_equal 0, q.size
      uow.requeue
      assert_equal 1, q.size
      assert_nil uow.acknowledge
    end

    it 'retrieves with strict setting' do
      fetch = Cone::BasicFetch.new(:queues => ['basic', 'bar', 'bar'], :strict => true)
      cmd = fetch.queues_cmd
      assert_equal cmd, ['queue:basic', 'queue:bar', Cone::BasicFetch::TIMEOUT]
    end

    it 'bulk requeues' do
      q1 = Cone::Queue.new('foo')
      q2 = Cone::Queue.new('bar')
      assert_equal 0, q1.size
      assert_equal 0, q2.size
      uow = Cone::BasicFetch::UnitOfWork
      Cone::BasicFetch.bulk_requeue([uow.new('fuzzy:queue:foo', 'bob'), uow.new('fuzzy:queue:foo', 'bar'), uow.new('fuzzy:queue:bar', 'widget')], {:queues => []})
      assert_equal 2, q1.size
      assert_equal 1, q2.size
    end

  end
end
