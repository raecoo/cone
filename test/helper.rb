$TESTING = true
# disable minitest/parallel threads
ENV["N"] = "0"

if ENV["COVERAGE"]
  require 'simplecov'
  SimpleCov.start do
    add_filter "/test/"
    add_filter "/myapp/"
  end
end
ENV['RACK_ENV'] = ENV['RAILS_ENV'] = 'test'

trap 'USR1' do
  threads = Thread.list

  puts
  puts "=" * 80
  puts "Received USR1 signal; printing all #{threads.count} thread backtraces."

  threads.each do |thr|
    description = thr == Thread.main ? "Main thread" : thr.inspect
    puts
    puts "#{description} backtrace: "
    puts thr.backtrace.join("\n")
  end

  puts "=" * 80
end

begin
  require 'pry-byebug'
rescue LoadError
end

require 'minitest/autorun'

require 'cone'
require 'cone/util'
Cone.logger.level = Logger::ERROR

Cone::Test = Minitest::Test

require 'cone/redis_connection'
REDIS_URL = ENV['REDIS_URL'] || 'redis://localhost/15'
REDIS = Cone::RedisConnection.create(:url => REDIS_URL, :namespace => 'testy')

Cone.configure_client do |config|
  config.redis = { :url => REDIS_URL, :namespace => 'testy' }
end

def capture_logging(lvl=Logger::INFO)
  old = Cone.logger
  begin
    out = StringIO.new
    logger = Logger.new(out)
    logger.level = lvl
    Cone.logger = logger
    yield
    out.string
  ensure
    Cone.logger = old
  end
end

def with_logging(lvl=Logger::DEBUG)
  old = Cone.logger.level
  begin
    Cone.logger.level = lvl
    yield
  ensure
    Cone.logger.level = old
  end
end
