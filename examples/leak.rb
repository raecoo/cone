# Making sure we do not leak memory

require 'cone'

redis = { :namespace => 'leak' }
Cone.configure_client { |config| config.redis = redis }
Cone.configure_server { |config| config.redis = redis }

$c = 0
$max = 10_000

# Start up cone via
# ./bin/cone -r ./examples/leak.rb > /dev/null
class MyWorker
  include Cone::Worker

  def perform
    $c += 1
    if $c % 100 == 0
      GC.start
      memory = `ps -o rss -p #{Process.pid}`.chomp.split("\n").last.to_i
      $stderr.puts "Using memory #{memory}"
    end
    if $c >= $max
      exit
    end
  end
end

# schedule some jobs to work on
$max.times { MyWorker.perform_async }
