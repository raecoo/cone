require 'cone'

# If your client is single-threaded, we just need a single connection in our Redis connection pool
Cone.configure_client do |config|
  config.redis = { :namespace => 'x', :size => 1 }
end

# Cone server is multi-threaded so our Redis connection pool size defaults to concurrency (-c)
Cone.configure_server do |config|
  config.redis = { :namespace => 'x' }
end

# Start up cone via
# ./bin/cone -r ./examples/por.rb
# and then you can open up an IRB session like so:
# irb -r ./examples/por.rb
# where you can then say
# PlainOldRuby.perform_async "like a dog", 3
#
class PlainOldRuby
  include Cone::Worker

  def perform(how_hard="super hard", how_long=1)
    sleep how_long
    puts "Workin' #{how_hard}"
  end
end
