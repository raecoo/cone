require 'cone'

module ActiveJob
  module QueueAdapters
    # == Cone adapter for Active Job
    #
    # Simple, efficient background processing for Ruby. Cone uses threads to
    # handle many jobs at the same time in the same process. It does not
    # require Rails but will integrate tightly with it to make background
    # processing dead simple.
    #
    # Read more about Cone {here}[http://cone.org].
    #
    # To use Cone set the queue_adapter config to +:cone+.
    #
    #   Rails.application.config.active_job.queue_adapter = :cone
    class ConeAdapter
      class << self
        def enqueue(job) #:nodoc:
          Cone::Client.push \
            'class' => JobWrapper,
            'wrapped' => job.class.to_s,
            'queue' => job.queue_name,
            'args'  => [ job.serialize ]
        end

        def enqueue_at(job, timestamp) #:nodoc:
          Cone::Client.push \
            'class' => JobWrapper,
            'wrapped' => job.class.to_s,
            'queue' => job.queue_name,
            'args'  => [ job.serialize ],
            'at'    => timestamp
        end
      end

      class JobWrapper #:nodoc:
        include Cone::Worker

        def perform(job_data)
          Base.execute job_data
        end
      end
    end
  end
end
