module Cone
  def self.hook_rails!

  end

  class Rails < ::Rails::Engine
    initializer 'cone' do
      Cone.hook_rails!
    end
  end if defined?(::Rails)
end
