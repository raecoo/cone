require 'cone'

module Cone
  module ExceptionHandler

    class Logger
      def call(ex, ctxHash)
        Cone.logger.warn(ctxHash) if !ctxHash.empty?
        Cone.logger.warn "#{ex.class.name}: #{ex.message}"
        Cone.logger.warn ex.backtrace.join("\n") unless ex.backtrace.nil?
      end

      # Set up default handler which just logs the error
      Cone.error_handlers << Cone::ExceptionHandler::Logger.new
    end

    def handle_exception(ex, ctxHash={})
      Cone.error_handlers.each do |handler|
        begin
          handler.call(ex, ctxHash)
        rescue => ex
          Cone.logger.error "!!! ERROR HANDLER THREW AN ERROR !!!"
          Cone.logger.error ex
          Cone.logger.error ex.backtrace.join("\n") unless ex.backtrace.nil?
        end
      end
    end

  end
end
