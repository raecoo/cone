#
# Simple middleware to save the current locale and restore it when the job executes.
# Use it by requiring it in your initializer:
#
#     require 'cone/middleware/i18n'
#
module Cone::Middleware::I18n
  # Get the current locale and store it in the message
  # to be sent to Cone.
  class Client
    def call(worker_class, msg, queue, redis_pool)
      msg['locale'] ||= I18n.locale
      yield
    end
  end

  # Pull the msg locale out and set the current thread to use it.
  class Server
    def call(worker, msg, queue)
      I18n.locale = msg['locale'] || I18n.default_locale
      yield
    ensure
      I18n.locale = I18n.default_locale
    end
  end
end

Cone.configure_client do |config|
  config.client_middleware do |chain|
    chain.add Cone::Middleware::I18n::Client
  end
end

Cone.configure_server do |config|
  config.client_middleware do |chain|
    chain.add Cone::Middleware::I18n::Client
  end
  config.server_middleware do |chain|
    chain.add Cone::Middleware::I18n::Server
  end
end
