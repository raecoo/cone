require 'connection_pool'
require 'redis'
require 'uri'

module Cone
  class RedisConnection
    class << self

      def create(options={})
        options[:url] ||= determine_redis_provider

        size = options[:size] || (Cone.server? ? (Cone.options[:concurrency] + 5) : 5)

        verify_sizing(size, Cone.options[:concurrency]) if Cone.server?

        pool_timeout = options[:pool_timeout] || 1
        log_info(options)

        ConnectionPool.new(:timeout => pool_timeout, :size => size) do
          build_client(options)
        end
      end

      private

      # Cone needs a lot of concurrent Redis connections.
      #
      # We need a connection for each Processor.
      # We need a connection for Pro's real-time change listener
      # We need a connection to various features to call Redis every few seconds:
      #   - the process heartbeat.
      #   - enterprise's leader election
      #   - enterprise's cron support
      def verify_sizing(size, concurrency)
        raise ArgumentError, "Your Redis connection pool is too small for Cone to work, your pool has #{size} connections but really needs to have at least #{concurrency + 2}" if size <= concurrency
      end

      def build_client(options)
        namespace = options[:namespace]

        client = Redis.new client_opts(options)
        if namespace
          begin
            require 'redis/namespace'
            Redis::Namespace.new(namespace, :redis => client)
          rescue LoadError
            Cone.logger.error("redis-namespace gem not included in Gemfile, cannot use namespace '#{namespace}'")
            exit(-127)
          end
        else
          client
        end
      end

      def client_opts(options)
        opts = options.dup
        if opts[:namespace]
          opts.delete(:namespace)
        end

        if opts[:network_timeout]
          opts[:timeout] = opts[:network_timeout]
          opts.delete(:network_timeout)
        end

        opts[:driver] = opts[:driver] || 'ruby'

        opts
      end

      def log_info(options)
        # Don't log Redis AUTH password
        redacted = "REDACTED"
        scrubbed_options = options.dup
        if scrubbed_options[:url] && (uri = URI.parse(scrubbed_options[:url])) && uri.password
          uri.password = redacted
          scrubbed_options[:url] = uri.to_s
        end
        if scrubbed_options[:password]
          scrubbed_options[:password] = redacted
        end
        if Cone.server?
          Cone.logger.info("Booting Cone #{Cone::VERSION} with redis options #{scrubbed_options}")
        else
          Cone.logger.debug("#{Cone::NAME} client with redis options #{scrubbed_options}")
        end
      end

      def determine_redis_provider
        ENV[ENV['REDIS_PROVIDER'] || 'REDIS_URL']
      end

    end
  end
end
