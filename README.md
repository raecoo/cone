Cone
==============

Cone is based on Sidekiq 4.0.3(`HEAD: 9bd9f66`). It dispatches jobs in a very specialized way for a very narrow set of problems. For example, if you don't need to manage a large number of rate-limited access to an external resource, you probably want to use Sidekiq and not Cone.

The problem Cone solves resembles that of a load balancing problem: only one worker
should be working on a work for a given user. Any other worker can work on any other user's
work, as only as no other worker is working on it. We also want to do this fairly so that every
user has a chance at getting work done steadily. This requires:

1. A queue for each user. (Which often means a lot of queues)
2. A turntable semaphore to control access to queues in a round-robin fashion
3. A way to track the status of each queue (active, empty, suspended) and when to push a queue back into the turntable


Installation
-----------------

``` gem 'cone' ```


Workers
-----------------

```
class APIWorker
  include Cone::Worker

  def perform(id)
    # something
  end
end

APIWorker.perform_async(api_token.id, item.id)
```

Sidekiq License
-----------------

Please see [LICENSE](https://github.com/mperham/sidekiq/blob/master/LICENSE) for licensing details.


Sidekiq Original Author
-----------------

Mike Perham, [@mperham](https://twitter.com/mperham) / [@sidekiq](https://twitter.com/sidekiq), [http://www.mikeperham.com](http://www.mikeperham.com) / [http://www.contribsys.com](http://www.contribsys.com
